package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage {
	
	@FindBy(css="#nav-top .notloggedin a:nth-child(1)")
	public WebElement logInLink;
	
	@FindBy(css="#nav-top .loggedin a:nth-child(3)")
	public WebElement logOutLink;
	
	public void clickLoginLink(){
		logInLink.click();
	}
	
	public void waitLogOutLinkVisible(WebDriver driver){
		logOutLink = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.visibilityOf(logOutLink));
	}
	
	public void clickToLogOut(){
		logOutLink.click();
	}

}
