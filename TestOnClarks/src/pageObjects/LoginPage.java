package pageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

	@FindBy(id="EmailAddress")
	public WebElement emailField;
	
	@FindBy(id="Password")
	public WebElement passwdField;
	
	@FindBy(id="MainContent_btnSignIn")
	public WebElement logInBttn;
	
	public void enterEmail(){
		emailField.sendKeys("Expearl3324@einrot.com");
	}
	
	public void enterPasswd(){
		passwdField.sendKeys("qwerty123");
	}
	
	public void submit(){
		logInBttn.sendKeys(Keys.ENTER);
	}
	
	
	
}
