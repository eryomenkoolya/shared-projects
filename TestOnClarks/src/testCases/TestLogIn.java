package testCases;

import org.testng.annotations.Test;
import org.testng.annotations.AfterMethod;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;

import pageObjects.HomePage;
import pageObjects.LoginPage;

public class TestLogIn {

	private static WebDriver driver;
	private HomePage homePage;
	private LoginPage loginPage;
	private String url = "http://www.clarks.co.uk/";
	
	@BeforeMethod
	public void openBrowser(){
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(url);
		
		homePage = PageFactory.initElements(driver, HomePage.class);
		loginPage = PageFactory.initElements(driver, LoginPage.class);
	}
	
	@Test
	public void logIn(){
		homePage.clickLoginLink();
		loginPage.enterEmail();
		loginPage.enterPasswd();
		loginPage.submit();
		homePage.waitLogOutLinkVisible(driver);
		homePage.clickToLogOut();
	}
	
	@AfterMethod
	public void closeBrowser(){
		driver.quit();
	}
}
